from django.contrib import admin

from .models import Item, Intor ,OrderItem, Order, Coupon, Refund, Address, UserProfile,Instagram,Journal


def make_refund_accepted(modeladmin, request, queryset):
    queryset.update(refund_requested=False, refund_granted=True)


make_refund_accepted.short_description = 'Update orders to refund granted'


class ItemAdmin(admin.ModelAdmin):
    list_display = ['id',
                    'title',
                    'photo_main',
                    'price',
                    'active',
                    'created_at',
                    'category',
                    ]
    list_display_links = [
        'title',
    ]
    list_filter = [ 'id',
                    'price',
                    'active',
                    'created_at',
                    'category',
                   ]
    search_fields = [
                    'price',
                    'active',
                    'created_at',
    ]


class OrderItemAdmin(admin.ModelAdmin):
    list_display = ['user',
                    'item',
                    'ordered',
                    'quantity',
                    ]
    list_display_links = [
        'user',
    ]
    list_filter = ['ordered',
                   ]
    search_fields = [
        'user__username',
    ]


class OrderAdmin(admin.ModelAdmin):
    list_display = ['user',
                    'ordered',
                    'shipping_address',
                    'ordered_date',
                    ]
    list_display_links = [
        'user',
        'shipping_address',
    ]
    list_filter = ['ordered',
                   'being_delivered',
                   'received',
                   ]
    search_fields = [
        'user__username',

    ]
    actions = [make_refund_accepted]


class AddressAdmin(admin.ModelAdmin):
    list_display = [
        'user',
        'street_address',
        'apartment_address',
        'zip',
        'address_type',
        'default'
    ]
    list_filter = ['default', 'address_type']
    search_fields = ['user', 'street_address', 'apartment_address', 'zip']


admin.site.register(Item,ItemAdmin)
admin.site.register(Intor)
admin.site.register(Instagram)
admin.site.register(Journal)
admin.site.register(OrderItem,OrderItemAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Coupon)
admin.site.register(Refund)
admin.site.register(Address, AddressAdmin)
admin.site.register(UserProfile)
