from django.conf import settings
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView, View
from django.shortcuts import redirect
from django.utils import timezone
from .forms import CheckoutForm, CouponForm, RefundForm, PaymentForm
from marketing.forms import EmailSignupForm
from .models import Item, Intor, OrderItem, Order, Address, Coupon, Refund, UserProfile ,Instagram,Journal
from blog.models import Post,Category
import random
import string

form = EmailSignupForm()


def create_ref_code():
    return ''.join(random.choices(string.ascii_lowercase + string.digits, k=20))


class HomeView(View):
    form = EmailSignupForm()

    def get(self, request, *args, **kwargs):
        product=Item.objects.all()
        intor = Intor.objects.all()
        instagram = Instagram.objects.all()
        journal =Journal.objects.all()
        featured = Post.objects.filter(featured=True)
        latest = Post.objects.order_by('-timestamp')[0:3]

        context = {
            'object_list': product,
            'intor':intor,
            'instagram':instagram,
            'journals':journal,
            'latest': latest,
            'form': self.form
        }
        return render(request, 'home.html', context)

    def post(self, request, *args, **kwargs):
        email = request.POST.get("email")
        new_signup = Signup()
        new_signup.email = email
        new_signup.save()
        messages.info(request, "با موفیت ثبت شد ")
        return redirect("home")



class ItemDetailView(DetailView):
    model = Item
    template_name = "product-detail.html"

    def get_context_data(self, **kwargs):
        context = super(ItemDetailView , self).get_context_data(**kwargs)
        context['product'] = Item.objects.all()
        context['order_item'] = OrderItem.objects.all()
        return context


def is_valid_form(values):
    valid = True
    for field in values:
        if field == '':
            valid = False
    return valid

    
class CheckoutView(View):
    def get(self, *args, **kwargs):
        form = CheckoutForm()
        order = Order.objects.get(user=self.request.user, ordered=False)
        context = {
            'order':order,
            'form':form
        }
        return render(self.request, "checkout.html", context)
    def post(self, *args, **kwargs):
        form = CheckoutForm(self.request.POST or None)
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            print(self.request.user)
            if form.is_valid():
                print(form.cleaned_data)
                frist_name =form.cleaned_data.get('frist_name')
                last_name =form.cleaned_data.get('last_name')
                phone =form.cleaned_data.get('phone')
                city =form.cleaned_data.get('city')
                note =form.cleaned_data.get('note')
                street_address =form.cleaned_data.get('street_address')
                apartment_address =form.cleaned_data.get('apartment_address')
                zip =form.cleaned_data.get('zip')
                payment_option =form.cleaned_data.get('payment_option')

                address = Address(
                    user=self.request.user,
                    frist_name=frist_name,
                    last_name=last_name,
                    phone=phone,
                    city=city,
                    note=note,
                    street_address=street_address,
                    apartment_address=apartment_address,
                    zip=zip
                )
                address.save()
                order.address = address
                order.save()
                messages.success(self.request, "سفارش شما ثبت شد ")
                return redirect('core:checkout')
            messages.error(self.request, "خطایی رخ داده")
            return redirect('core:checkout')
        except ObjectDoesNotExist:
            messages.warning(self.request, "سبد خرید شما خالی است ")
            return redirect("core:order-summary")





class OrderSummaryView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'order': order
            }
            return render(self.request, 'order_summary.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "سبد خرید شما خالی است")
            return redirect("/")

@login_required
def add_to_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)
    order_item, created = OrderItem.objects.get_or_create(
        item=item,
        user=request.user,
        ordered=False
    )
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item.quantity += 1
            order_item.save()
            messages.info(request, "به تعداد محصول افزوده شد ")
            return redirect("core:order-summary")
        else:
            order.items.add(order_item)
            messages.success(request, "به سبد خرید اضافه شد ")
            return redirect("core:order-summary")
    else:
        ordered_date = timezone.now()
        order = Order.objects.create(
            user=request.user, ordered_date=ordered_date)
        order.items.add(order_item)
        messages.success(request, "به سبد خرید اضافه شد ")
        return redirect("core:order-summary")


@login_required
def remove_from_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            order.items.remove(order_item)
            
            messages.info(request, "از سبد خرید حذف شد ")
            return redirect("core:order-summary")
        else:
            messages.error(request, "این محصول در سبد خرید شما نیست")
            return redirect("core:product-detail", slug=slug)
    else:
        messages.error(request, "سبد خرید شما خالی است")
        return redirect("core:product-detail", slug=slug)


@login_required
def remove_single_item_from_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            if order_item.quantity > 1:
                order_item.quantity -= 1
                order_item.save()
            else:
                order.items.remove(order_item)
            messages.info(request, "از تعداد محصول کم شد")
            return redirect("core:order-summary")
        else:
            messages.info(request, "این محصول در سبد خرید شما نیست")
            return redirect("core:product-detail", slug=slug)
    else:
        messages.info(request, "سبد خرید شما خالی است")
        return redirect("core:product-detail", slug=slug)


def get_coupon(request, code):
    try:
        coupon = Coupon.objects.get(code=code)
        return coupon
    except ObjectDoesNotExist:
        messages.info(request, "This coupon does not exist")
        return redirect("core:checkout")


class AddCouponView(View):
    def post(self, *args, **kwargs):
        form = CouponForm(self.request.POST or None)
        if form.is_valid():
            try:
                code = form.cleaned_data.get('code')
                order = Order.objects.get(
                    user=self.request.user, ordered=False)
                order.coupon = get_coupon(self.request, code)
                order.save()
                messages.success(self.request, "Successfully added coupon")
                return redirect("core:checkout")
            except ObjectDoesNotExist:
                messages.info(self.request, "You do not have an active order")
                return redirect("core:checkout")


class RequestRefundView(View):
    def get(self, *args, **kwargs):
        form = RefundForm()
        context = {
            'form': form
        }
        return render(self.request, "request_refund.html", context)

    def post(self, *args, **kwargs):
        form = RefundForm(self.request.POST)
        if form.is_valid():
            ref_code = form.cleaned_data.get('ref_code')
            message = form.cleaned_data.get('message')
            email = form.cleaned_data.get('email')
            # edit the order
            try:
                order = Order.objects.get(ref_code=ref_code)
                order.refund_requested = True
                order.save()

                # store the refund
                refund = Refund()
                refund.order = order
                refund.reason = message
                refund.email = email
                refund.save()

                messages.info(self.request, "Your request was received.")
                return redirect("core:request-refund")

            except ObjectDoesNotExist:
                messages.info(self.request, "This order does not exist.")
                return redirect("core:request-refund")
