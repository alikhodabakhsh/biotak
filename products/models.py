from django.db.models.signals import post_save, pre_save
from django.conf import settings
from django.db import models
from django.db.models import Sum
from django.shortcuts import reverse

from tinymce import HTMLField
from furniture.utils import id_generator, unique_slug_generator


CATEGORY_CHOICES = (
    ('S', 'مبلمان'),
    ('CH', 'صندلی'),
    ('T', 'میز')
)

LABEL_CHOICES = (
    ('S', 'حراج'),
    ('N', 'جدید'),
    ('L', 'لاکچری')
)

ADDRESS_CHOICES = (
    ('B', 'آپارتمان'),
    ('S', 'ارسال به آدرس دیگر'),
)


class UserProfile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='کاربر')
    one_click_purchasing = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'پروفایل'
        verbose_name_plural = ' پروفایل کاربرها'

    def __str__(self):
        return self.user.username


class ProductManager(models.Manager):
    def all(self):
        return self.filter(active=True)

    def available_products(self):
        return self.all().filter(sizes__available_count__gt=0).distinct()


class Intor(models.Model):
    title = models.CharField(max_length=120, verbose_name='عنوان')
    slug = models.SlugField(max_length=200, unique=True, verbose_name='لینک')
    photo = models.ImageField(
        upload_to='intor_photos/%Y/%m/%d/', verbose_name='عکس')

    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='ایجاد شده در')

    class Meta:
        verbose_name = ' معرفی محصول'
        verbose_name_plural = ' معرفی محصولات'

    def __str__(self):
        return self.title


class Instagram(models.Model):
    title = models.CharField(max_length=120, verbose_name='عنوان')
    slug = models.SlugField(max_length=200, unique=True, verbose_name='لینک')
    description = models.TextField(max_length=50, verbose_name='توضیحات')
    photo = models.ImageField(
        upload_to='instagram_photos/%Y/%m/%d/', verbose_name='عکس')

    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='ایجاد شده در')

    class Meta:
        verbose_name = 'اینستاگرام'
        verbose_name_plural = 'اینستاگرام '

    def __str__(self):
        return self.title


class Journal(models.Model):
    title = models.CharField(max_length=120, verbose_name='عنوان')
    slug = models.SlugField(max_length=200, unique=True, verbose_name='لینک')
    description = models.TextField(verbose_name='توضیحات')
    photo = models.ImageField(
        upload_to='product_photos/%Y/%m/%d/', verbose_name='عکس')

    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='ایجاد شده در')

    class Meta:
        verbose_name = 'ژورنال'
        verbose_name_plural = 'ژورنال '

    def __str__(self):
        return self.title


class Item(models.Model):
    title = models.CharField(max_length=120, verbose_name='عنوان')
    slug = models.SlugField(max_length=200, unique=True, verbose_name='لینک')
    description = models.TextField(max_length=500, verbose_name='توضیحات')
    review = HTMLField(blank=True, null=True, verbose_name='نقد و بررسی')
    photo_main = models.ImageField(
        upload_to='product_photos/%Y/%m/%d/', verbose_name='عکس محصول')
    photo_1 = models.ImageField(
        upload_to='product_photos/%Y/%m/%d/', blank=True, null=True, verbose_name='عکس 1')
    photo_2 = models.ImageField(
        upload_to='product_photos/%Y/%m/%d/', blank=True, null=True, verbose_name='عکس 2')
    photo_3 = models.ImageField(
        upload_to='product_photos/%Y/%m/%d/', blank=True, null=True, verbose_name='عکس 3')
    price = models.PositiveIntegerField(
        null=True, default=0, verbose_name='قیمت')
    discount_price = models.PositiveIntegerField(
        null=True, blank=True, default=0, verbose_name='قیمت با تخفیف')
    active = models.BooleanField(default=True, verbose_name='وضعیت')
    code = models.CharField(max_length=40, unique=True,
                            editable=False, blank=True, null=True)
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name=' ایجاد شده در ')
    category = models.CharField(
        choices=CATEGORY_CHOICES, max_length=2, verbose_name='دسته بندی')
    label = models.CharField(
        choices=LABEL_CHOICES, blank=True, null=True, max_length=1, verbose_name='برچسب')

    objects = ProductManager()

    class Meta:
        verbose_name = 'محصول'
        verbose_name_plural = 'محصولات'
        ordering = ('-created_at',)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.code = id_generator()
        super(Item, self).save(*args, **kwargs)

    @property
    def discount_percent(self):
        if self.discount_price:
            discount_percent = 100 - (self.discount_price * 100) / self.price
            return int(discount_percent)
        return

    @property
    def final_price(self):
        if self.discount_price:
            return self.discount_price
        return self.price

    @property
    def customer_profit(self):
        if self.discount_price:
            return self.price - self.discount_price
        return 0

    def get_absolute_url(self):
        return reverse("core:product-detail", kwargs={
            'slug': self.slug
        })

    def get_add_to_cart_url(self):
        return reverse("core:add-to-cart", kwargs={
            'slug': self.slug
        })

    def get_remove_from_cart_url(self):
        return reverse("core:remove-from-cart", kwargs={
            'slug': self.slug
        })


def slug_generator(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(slug_generator, sender=Item)


class OrderItem(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, verbose_name='کاربر')
    ordered = models.BooleanField(default=False, verbose_name='وضعیت سفارش')
    item = models.ForeignKey(
        Item, on_delete=models.CASCADE, verbose_name='محصول')
    quantity = models.IntegerField(default=1, verbose_name='تعداد')

    class Meta:
        verbose_name = 'سفارش محصول'
        verbose_name_plural = ' سفارش محصولات'

    def __str__(self):
        return f"{self.quantity} of {self.item.title}"

    def get_total_item_price(self):
        return self.quantity * self.item.price

    def get_total_discount_item_price(self):
        return self.quantity * self.item.discount_price

    def get_amount_saved(self):
        return self.get_total_item_price() - self.get_total_discount_item_price()

    def get_final_price(self):
        if self.item.discount_price:
            return self.get_total_discount_item_price()
        return self.get_total_item_price()


class Order(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, verbose_name='کاربر')
    items = models.ManyToManyField(OrderItem, verbose_name='محصولات')
    start_date = models.DateTimeField(auto_now_add=True, verbose_name='ثبت در')
    ordered_date = models.DateTimeField(verbose_name='ثبت سفارش در')
    ordered = models.BooleanField(default=False, verbose_name='وضعیت سفارش ')
    shipping_address = models.ForeignKey(
        'Address', related_name='shipping_address', on_delete=models.SET_NULL, blank=True, null=True, verbose_name='آدرس سفارش ')
    billing_address = models.ForeignKey(
        'Address', related_name='billing_address', on_delete=models.SET_NULL, blank=True, null=True, verbose_name='آدرس آپارتمان')
    payment = models.ForeignKey(
        'Payment', on_delete=models.SET_NULL, blank=True, null=True, verbose_name='وضعیت پرداخت ')
    coupon = models.ForeignKey(
        'Coupon', on_delete=models.SET_NULL, blank=True, null=True, verbose_name='کد تخفیف ')
    being_delivered = models.BooleanField(
        default=False, verbose_name='آماده کردن سفارش ')
    received = models.BooleanField(default=False, verbose_name=' تحویل سفارش ')

    '''
    1. Item added to cart
    2. Adding a billing address
    (Failed checkout)
    3. Payment
    (Preprocessing, processing, packaging etc.)
    4. Being delivered
    5. Received
    6. Refunds
    '''

    class Meta:
        verbose_name = 'ثبت سفارش'
        verbose_name_plural = 'سفارشات ثبت شده'

    def __str__(self):
        return self.user.username

    def get_total(self):
        total = 0
        for order_item in self.items.all():
            total += order_item.get_final_price()
        if self.coupon:
            total -= self.coupon.amount
        return total


class Address(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, verbose_name='کاربر')
    frist_name = models.CharField(max_length=50, verbose_name='نام')
    last_name = models.CharField(max_length=50, verbose_name='نام خانوادگی')
    phone = models.CharField(max_length=50, verbose_name='شماره موبایل')
    city = models.CharField(max_length=50, verbose_name='شهر')
    note = models.CharField(max_length=50, verbose_name='یادداشت')
    street_address = models.CharField(
        max_length=100, verbose_name='آدرس سفارش')
    apartment_address = models.CharField(
        max_length=100, verbose_name='آدرس آپارتمان')
    zip = models.CharField(max_length=100, verbose_name='کد پستی')
    address_type = models.CharField(
        max_length=1, choices=ADDRESS_CHOICES, verbose_name='نوع آدرس')
    default = models.BooleanField(default=False, verbose_name=' وضعیت')

    class Meta:
        verbose_name = 'آدرس'
        verbose_name_plural = 'آدرس ها'

    def __str__(self):
        return self.user.username


class Payment(models.Model):
    stripe_charge_id = models.CharField(max_length=50, verbose_name='شناسه')
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.SET_NULL, blank=True, null=True, verbose_name=' کاربر')
    amount = models.FloatField(verbose_name=' مقدار')
    timestamp = models.DateTimeField(
        auto_now_add=True, verbose_name=' ایجاد شده در')

    def __str__(self):
        return self.user.username


class Coupon(models.Model):
    code = models.CharField(max_length=15, verbose_name='کد')
    amount = models.FloatField(verbose_name='مقدار')

    class Meta:
        verbose_name = 'تخفیف'
        verbose_name_plural = ' تخفیف ها'

    def __str__(self):
        return self.code


class Refund(models.Model):
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE, verbose_name='سفارش')
    reason = models.TextField(verbose_name='دلیل')
    accepted = models.BooleanField(default=False, verbose_name='وضعیت پذیرش')
    email = models.EmailField(verbose_name='ایمیل')

    class Meta:
        verbose_name = 'سفارش برگشت خورده'
        verbose_name_plural = 'سفارشات برگشت خورده'

    def __str__(self):
        return f"{self.pk}"


def userprofile_receiver(sender, instance, created, *args, **kwargs):
    if created:
        userprofile = UserProfile.objects.create(user=instance)


post_save.connect(userprofile_receiver, sender=settings.AUTH_USER_MODEL)
