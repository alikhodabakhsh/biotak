# Generated by Django 3.0.1 on 2020-01-18 15:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0005_auto_20200118_1906'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='address',
            options={'verbose_name': 'آدرس', 'verbose_name_plural': 'آدرس ها'},
        ),
    ]
