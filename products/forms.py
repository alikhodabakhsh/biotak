from django import forms



PAYMENT_CHOICES = (
    ('S', 'آنلاین'),
    ('P', 'مستقیم')
)


class CheckoutForm(forms.Form):

    frist_name = forms.CharField()
    last_name= forms.CharField()
    phone = forms.CharField()
    city = forms.CharField()
    note = forms.CharField(required=False)
    street_address = forms.CharField()
    apartment_address = forms.CharField(required=False)
    zip = forms.CharField()

    
    set_default_shipping = forms.BooleanField(required=False)
    use_default_shipping = forms.BooleanField(required=False)
 

    payment_option = forms.ChoiceField(
        widget=forms.RadioSelect, choices=PAYMENT_CHOICES)


class CouponForm(forms.Form):
    code = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Promo code',
        'aria-label': 'Recipient\'s username',
        'aria-describedby': 'basic-addon2'
    }))


class RefundForm(forms.Form):
    ref_code = forms.CharField()
    message = forms.CharField(widget=forms.Textarea(attrs={
        'rows': 4
    }))
    email = forms.EmailField()


class PaymentForm(forms.Form):
    stripeToken = forms.CharField(required=False)
    save = forms.BooleanField(required=False)
    use_default = forms.BooleanField(required=False)
