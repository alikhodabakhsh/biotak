from django.db import models
from django.contrib.auth.models import User


class Activation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE ,blank=True)
    created_at = models.DateTimeField(auto_now_add=True,blank=True)
    code = models.CharField(max_length=20, unique=True, blank=True)
    email = models.EmailField(blank=True)
