from django.contrib import admin

from .models import Author, Category, Post, Comment, PostView

class AuthorAdmin(admin.ModelAdmin):
    list_display = ['user',
                    ]
    list_display_links = [
        'user',
    ]
    search_fields = [
        'user__username',
    ]


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['title',
                    ]


class PostAdmin(admin.ModelAdmin):
    list_display = ['title',
                    'author',
                    'timestamp',
                    'featured',
                    ]
    list_display_links = [
        'title',
        'author',
    ]
    search_fields = [
        'title',
        'author',
        'timestamp',
        'featured',
    ]

class CommentAdmin(admin.ModelAdmin):
    list_display = ['user',
                    'post',
                    'timestamp',
                    ]
    list_display_links = [
        'post',
    ]
    search_fields = [
        'user',
        'post',
        'timestamp',
    ]

class PostViewAdmin(admin.ModelAdmin):
    list_display = ['user',
                    'post',
                    ]
    list_display_links = [
        'post',
    ]
    search_fields = [
        'user',
        'post',
    ]

admin.site.register(Author,AuthorAdmin)
admin.site.register(Category,CategoryAdmin)
admin.site.register(Post,PostAdmin)
admin.site.register(Comment,CommentAdmin)
admin.site.register(PostView,PostViewAdmin)