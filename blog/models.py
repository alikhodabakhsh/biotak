from tinymce import HTMLField
from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse

User = get_user_model()


class PostView(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,verbose_name='نام کاربری')
    post = models.ForeignKey('Post', on_delete=models.CASCADE,verbose_name='پست')

    class Meta:
        verbose_name='نمایش پست'
        verbose_name_plural='نمایش پست ها '

    def __str__(self):
        return self.user.username


class Author(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,verbose_name='نام کاربری')
    profile_picture = models.ImageField(upload_to='profile_picture/%Y/%m/%d/', blank=True, null=True,verbose_name='عکس')
    name=models.CharField(max_length=50,verbose_name='نام')

    class Meta:
        verbose_name='نویسنده'
        verbose_name_plural='نویسنده ها'

    def __str__(self):
        return self.user.username


class Category(models.Model):
    title = models.CharField(max_length=20,verbose_name='عنوان')

    class Meta:
        verbose_name='دسته بندی'
        verbose_name_plural='دسته بندی ها'

    def __str__(self):
        return self.title


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,verbose_name='نام کاربری')
    timestamp = models.DateTimeField(auto_now_add=True,verbose_name='ایجاد شده در')
    content = models.TextField()
    post = models.ForeignKey(
        'Post', related_name='comments', on_delete=models.CASCADE,verbose_name='پست')

    class Meta:
        verbose_name='نظر'
        verbose_name_plural='نظرات'

    def __str__(self):
        return self.user.username


class Post(models.Model):
    title = models.CharField(max_length=100,verbose_name='عنوان')
    overview = models.TextField(verbose_name='پیش نمایش')
    timestamp = models.DateTimeField(auto_now_add=True,verbose_name='ایجاد شده در')
    content = HTMLField(verbose_name='متن مقاله')
    # comment_count = models.IntegerField(default = 0)
    # view_count = models.IntegerField(default = 0)
    author = models.ForeignKey(Author, on_delete=models.CASCADE,verbose_name='نویسنده ')
    thumbnail = models.ImageField(upload_to='thumbnail/%Y/%m/%d/',verbose_name='عکس مقاله')
    categories = models.ManyToManyField(Category,verbose_name='دسته بندی')
    featured = models.BooleanField(verbose_name='ویژه')
    previous_post = models.ForeignKey(
        'self', related_name='previous', on_delete=models.SET_NULL, blank=True, null=True ,verbose_name='پست قبلی ')
    next_post = models.ForeignKey(
        'self', related_name='next', on_delete=models.SET_NULL, blank=True, null=True ,verbose_name='پست بعدی ')

    class Meta:
        verbose_name='پست'
        verbose_name_plural='پست ها'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:post-detail', kwargs={
            'pk': self.pk
        })


    @property
    def get_comments(self):
        return self.comments.all().order_by('-timestamp')

    @property
    def comment_count(self):
        return Comment.objects.filter(post=self).count()

    @property
    def view_count(self):
        return PostView.objects.filter(post=self).count()
