from django.urls import path
from .views import (
    search,
    IndexView,
    PostListView,
    PostDetailView,
)
from marketing.views import email_list_signup

app_name = 'blog'

urlpatterns = [
    path('post/list/', PostListView.as_view(), name='post-list'),
    path('search/', search, name='search'),
    path('email-signup/', email_list_signup, name='email-list-signup'),
    path('post/<pk>/', PostDetailView.as_view(), name='post-detail'),

    
]
